<?php

namespace Drupal\learning;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for course.
 */
class CourseTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
