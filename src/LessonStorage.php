<?php

namespace Drupal\learning;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\learning\Entity\LessonInterface;

/**
 * Defines the storage handler class for Lesson entities.
 *
 * This extends the base storage class, adding required special handling for
 * Lesson entities.
 *
 * @ingroup learning
 */
class LessonStorage extends SqlContentEntityStorage implements LessonStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(LessonInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {lesson_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {lesson_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(LessonInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {lesson_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('lesson_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
