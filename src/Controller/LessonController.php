<?php

namespace Drupal\learning\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\learning\Entity\LessonInterface;

/**
 * Class LessonController.
 *
 *  Returns responses for Lesson routes.
 */
class LessonController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Lesson  revision.
   *
   * @param int $lesson_revision
   *   The Lesson  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($lesson_revision) {
    $lesson = $this->entityManager()->getStorage('lesson')->loadRevision($lesson_revision);
    $view_builder = $this->entityManager()->getViewBuilder('lesson');

    return $view_builder->view($lesson);
  }

  /**
   * Page title callback for a Lesson  revision.
   *
   * @param int $lesson_revision
   *   The Lesson  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($lesson_revision) {
    $lesson = $this->entityManager()->getStorage('lesson')->loadRevision($lesson_revision);
    return $this->t('Revision of %title from %date', ['%title' => $lesson->label(), '%date' => format_date($lesson->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Lesson .
   *
   * @param \Drupal\learning\Entity\LessonInterface $lesson
   *   A Lesson  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(LessonInterface $lesson) {
    $account = $this->currentUser();
    $langcode = $lesson->language()->getId();
    $langname = $lesson->language()->getName();
    $languages = $lesson->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $lesson_storage = $this->entityManager()->getStorage('lesson');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $lesson->label()]) : $this->t('Revisions for %title', ['%title' => $lesson->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all lesson revisions") || $account->hasPermission('administer lesson entities')));
    $delete_permission = (($account->hasPermission("delete all lesson revisions") || $account->hasPermission('administer lesson entities')));

    $rows = [];

    $vids = $lesson_storage->revisionIds($lesson);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\learning\LessonInterface $revision */
      $revision = $lesson_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $lesson->getRevisionId()) {
          $link = $this->l($date, new Url('entity.lesson.revision', ['lesson' => $lesson->id(), 'lesson_revision' => $vid]));
        }
        else {
          $link = $lesson->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.lesson.translation_revert', ['lesson' => $lesson->id(), 'lesson_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.lesson.revision_revert', ['lesson' => $lesson->id(), 'lesson_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.lesson.revision_delete', ['lesson' => $lesson->id(), 'lesson_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['lesson_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
