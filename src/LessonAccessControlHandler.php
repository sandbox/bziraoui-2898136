<?php

namespace Drupal\learning;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Lesson entity.
 *
 * @see \Drupal\learning\Entity\Lesson.
 */
class LessonAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\learning\Entity\LessonInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished lesson entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published lesson entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit lesson entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete lesson entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add lesson entities');
  }

}
