<?php

namespace Drupal\learning\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Course entities.
 *
 * @ingroup learning
 */
class CourseDeleteForm extends ContentEntityDeleteForm {


}
