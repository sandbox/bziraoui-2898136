<?php

namespace Drupal\learning\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Quiz entities.
 *
 * @ingroup learning
 */
class QuizDeleteForm extends ContentEntityDeleteForm {


}
