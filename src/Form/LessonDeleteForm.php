<?php

namespace Drupal\learning\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Lesson entities.
 *
 * @ingroup learning
 */
class LessonDeleteForm extends ContentEntityDeleteForm {


}
