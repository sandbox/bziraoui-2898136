<?php

namespace Drupal\learning;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\learning\Entity\LessonInterface;

/**
 * Defines the storage handler class for Lesson entities.
 *
 * This extends the base storage class, adding required special handling for
 * Lesson entities.
 *
 * @ingroup learning
 */
interface LessonStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Lesson revision IDs for a specific Lesson.
   *
   * @param \Drupal\learning\Entity\LessonInterface $entity
   *   The Lesson entity.
   *
   * @return int[]
   *   Lesson revision IDs (in ascending order).
   */
  public function revisionIds(LessonInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Lesson author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Lesson revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\learning\Entity\LessonInterface $entity
   *   The Lesson entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(LessonInterface $entity);

  /**
   * Unsets the language for all Lesson with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
