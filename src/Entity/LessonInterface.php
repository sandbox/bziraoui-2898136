<?php

namespace Drupal\learning\Entity;

use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Lesson entities.
 *
 * @ingroup learning
 */
interface LessonInterface extends RevisionableInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Lesson name.
   *
   * @return string
   *   Name of the Lesson.
   */
  public function getName();

  /**
   * Sets the Lesson name.
   *
   * @param string $name
   *   The Lesson name.
   *
   * @return \Drupal\learning\Entity\LessonInterface
   *   The called Lesson entity.
   */
  public function setName($name);

  /**
   * Gets the Lesson creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Lesson.
   */
  public function getCreatedTime();

  /**
   * Sets the Lesson creation timestamp.
   *
   * @param int $timestamp
   *   The Lesson creation timestamp.
   *
   * @return \Drupal\learning\Entity\LessonInterface
   *   The called Lesson entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Lesson published status indicator.
   *
   * Unpublished Lesson are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Lesson is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Lesson.
   *
   * @param bool $published
   *   TRUE to set this Lesson to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\learning\Entity\LessonInterface
   *   The called Lesson entity.
   */
  public function setPublished($published);

  /**
   * Gets the Lesson revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Lesson revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\learning\Entity\LessonInterface
   *   The called Lesson entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Lesson revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Lesson revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\learning\Entity\LessonInterface
   *   The called Lesson entity.
   */
  public function setRevisionUserId($uid);

}
