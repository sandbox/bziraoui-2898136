<?php

/**
 * @file
 * Contains lesson.page.inc.
 *
 * Page callback for Lesson entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Lesson templates.
 *
 * Default template: lesson.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_lesson(array &$variables) {
  // Fetch Lesson Entity Object.
  $lesson = $variables['elements']['#lesson'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
